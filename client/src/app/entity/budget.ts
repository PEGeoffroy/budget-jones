import { Operation } from "./operation";

export interface Budget {
  id: number;
  name: string;
  sum: number;
  operations: Array<Operation>;
  total?: number;
  totalInput?: number;
  totalOutput?: number
}
