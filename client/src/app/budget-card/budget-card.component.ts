import { Component, OnInit } from '@angular/core';
import { Budget } from '../entity/budget';
import { BudgetService } from '../service/budget.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-budget-card',
  templateUrl: './budget-card.component.html',
  styleUrls: ['./budget-card.component.css']
})
export class BudgetCardComponent implements OnInit {
  id:number;
  budgetTotalInput: number;
  budgetTotalOutput: number;
  total : number;
  budget: Budget;

  constructor(private service: BudgetService,route: ActivatedRoute) {
    route.params.subscribe(data => this.id = +data['id'])
  }

  ngOnInit() {
    this.service.findById(this.id).subscribe((response)=>{
      this.budget =response;
    });
    // this.service.getTotalInput(this.id).subscribe(response=>this.budgetTotalInput =response);
    // this.service.getTotalOutput(this.id).subscribe(response=>this.budgetTotalOutput =response);
    // this.service.getTotal(this.id).subscribe(response=>this.total =response);
    
    
  }
}
