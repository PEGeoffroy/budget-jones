import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../entity/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  private urlSql = "http://localhost:8080";
  private url = `${this.urlSql}/user`;

  findByid(id:number):Observable<User> {

    return this.http.get<User>(`${this.url}/${id}`);
  }

  add(user:User):Observable<User>{

    return this.http.post<User>(this.url, user);
  }

  delete(id:number):Observable<any>{

    return this.http.delete(`${this.url}/${id}`)
  }

  update(user:User):Observable<User> {
    return this.http.put<User>(`${this.url}/${user.id}`, user);
  }

  getAllBudgets(user:User):Observable<any>{
    return this.http.get(`{this.url}/AllBudgets`)
  }
}

