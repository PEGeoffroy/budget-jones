import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Budget } from '../entity/budget';
import { Observable } from 'rxjs';
import { mergeAll, map, toArray, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BudgetService {
  private urlSql = "http://localhost:8080/";
  private url = `${this.urlSql}budget/`;


  constructor(private http: HttpClient) { }

  findAll():Observable<Budget[]> {
    return this.http.get<Budget[]>(this.url);
    /*.pipe(
      mergeAll(), //flatten the array (transforme l'observable<Operation[]> en observable<Operation>)
      mergeMap(budget => this.getTotal(budget.id),  //pour chaque operation, il fait un nouvel observable pour le budget en utilisant l'id du budget
      (budget, total) => {
        budget.total = total["SUM(operation.sum)"]; // ajoute le total récupéré par la deuxième requête à la réponse de la première requête
        return budget;
      }),
      toArray() // on repasse l'observable en tableau
    );*/
  }

  findById(id:number):Observable<Budget> {

    return this.http.get<Budget>(this.url + id);
  }

  add(budget:Budget):Observable<Budget>{

    return this.http.post<Budget>(this.url, budget);
  }

  delete(id:number):Observable<any>{

    return this.http.delete(this.url + id)
  }

  update(budget:Budget):Observable<Budget> {

    return this.http.put<Budget>(this.url + budget.id, budget);
  }

  getTotal(id :number):Observable<any> {
    return this.http.get(`${this.url}${id}/total`);
  }

  getTotalInput(id :number):Observable<any>{
    return this.http.get(`${this.url}${id}/total-input`);
  }
  
  getTotalOutput(id :number):Observable<any>{
    return this.http.get(`${this.url}${id}/total-output`);
  }
}
